﻿#!/usr/bin/env python3
"""Functions for reading and working with VRPTW text input files."""

import os.path, math

def read_string_list(filename='r101', skipFirstLine=True):
    """Reads a file and returns a list of strings, containing each line as string entry"""
    #default extension should be .txt
    name, ext = os.path.splitext(filename)
    if ext == '': ext = '.txt'
    filename = name + ext
    #open file and return list of strings
    list = []; firstLine = True; i = 0
    with open(filename, 'r', encoding='utf-8') as f:
        for line in f:
            if firstLine and skipFirstLine:
                firstLine = False
                continue
            list.append(line.strip())
    return list

def read_customer_list_from_file(filename):
    """Reads a customer text file (first line is header) and returns a list of customer dictionaries"""
    lines = read_string_list(filename, skipFirstLine=False)
    keys = lines[0].split()
    vals = [[__to_int_or_float_if_possible(v) for v in l.split()] for l in lines[1:]]
    return [dict(zip(keys, v)) for v in vals]

def __to_int_or_float_if_possible(val):
    """Converts a string to a floating point value if possible, if not leave it as str"""
    #is all int?
    try:
        return int(val)
    except ValueError:
        #maybe it's a float
        try:
            return float(val)
        except ValueError:
            #ok, its a string
            return val


def get_demand(entries, cust_no):
    """The function has to return that customer's demand as floating point number.

    entries: a list of strings - one for each entry
    :param cust_no customer number CUST_NO (integer)"""
    cust_no = str(cust_no)
    for entry in entries:
        splittedLine = __splitLine(entry) #FIXME delete newline char at the end of the str
        if splittedLine[0] == cust_no:
            return float(splittedLine[3])

def __get_coord(entries, cust_no):
    """Like `get_demand(,)` - but returns the x and the y coordinate as (x,y)"""
    cust_no = str(cust_no)
    for entry in entries:
        splittedLine = __splitLine(entry)
        if splittedLine[0] == cust_no:
            return float(splittedLine[1]), float(splittedLine[2])

def calc_distance(entries, cust_no_1, cust_no_2):
    """return the euclidean distance between the two customers

    cust_no_1 and cust_no_2: customer numbers of both
    """
    x1, y1 = __get_coord(entries, cust_no_1)
    x2, y2 = __get_coord(entries, cust_no_2)
    return math.sqrt( (x1 - x2)**2 + (y1 - y2)**2 )


def __splitLine(str, sep=' '):
    """Like str.split(), but separator that occur a bunch of times are ignored after the first one"""
    list = str.split(sep)
    newList = []
    for elem in list:
        if len(elem) > 0:
            newList.append(elem)
    return newList

if __name__ == '__main__':
    #x = calc_distance(read_string_list(), 1, 3)
    #print(x)
    l = read_string_list()
    print(l)