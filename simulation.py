﻿#!/usr/bin/env python3

"""
This simulation for a bike shop shows us the total demand for the next 10 days.
"""
import argparse
from random import random, choice
import sys


class ProbabilityDistributionError(Exception):
    """
    Exception used for distributions with probabilities not adding up to 1.
    """

def read_distribution(filename):
    """
    Read the file with the given name and return a probability distribution.

    The probability distribution is returned in a dictionary. It is expected
    that the given probabilities add up to 1.
    """
    distribution = {}
    with open(filename, encoding='utf-8') as f:
        f.readline()  # skip the header line
        for line in f:
            if len(line):
                demand, prob = line.split()
                demand, prob = int(demand), float(prob)
                distribution[demand] = prob
    if sum(distribution.values()) != 1:
        raise ProbabilityDistributionError
    return distribution

def simulate_total_demand(distribution, days):
    """
    Simulates the total demand of the next `days` considering a given `distribution` table
    This simulation simulates every day unaffected and returns these day-simulations cumulated as a number of demand

    :param distribution: a dictionary. key is the demand, value is the probabilty. example: {1: 0.5, 2: 0.5} -> 1 and two are equally distributed
                         the sum of the values has to be equal to 1; only percentages like 0.33 are allowed! 0.333 is not allowed! (max amount of decimal numbers: 2)
    :param days: amount of days (int)
    """
    assert sum(distribution.values()) == 1
    assert days >= 0
    #create a list of tuples like [ (demand, probabilty), (demand, probability) ]
    weighted_choices = [(demand, int(p*100)) for demand, p in distribution.items() ]
    #create a list of demands containing 100 elements. every demand is included as often as its probability times 100 percentage
    population = [val for val, cnt in weighted_choices for i in range(cnt)]
    #do the simulations:
    cum = 0
    for i in range(days):
        day_simulation = choice(population)
        cum += day_simulation
    return cum


def main():
    """
    Run the given simulation from the command-line.
    """
    parser = argparse.ArgumentParser(
        description="Simulate future demand for bikes.")
    parser.add_argument("--filename", help="the input filename",
                        default="distribution.txt")
    parser.add_argument("--days", help="the number of days to simulate",
                        default=10, type=int)
    args = parser.parse_args()
    distribution = read_distribution(args.filename)
    total_demand = simulate_total_demand(distribution, args.days)
    print("The total demand for the next {} days is simulated to be {} bikes."
          .format(args.days, total_demand))


if __name__ == "__main__":
    sys.exit(main())
