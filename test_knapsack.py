"""
Test for the task knapsack.py
"""

import unittest
import knapsack
from knapsack import Good
from collections import namedtuple

class TestKnapsack(unittest.TestCase):
    """Test class for testing module comprehensions."""
    
    
    def test_knapsack(self):
        """This function is a test for the knapsack optimal solution."""
        goods = [Good(4, 5), Good(3, 4), Good(3, 2), Good(2, 1)]  # sample data
        capacity = 6
        self.assertEqual(knapsack.calc_profit(goods, capacity), (6, [Good(profit=3, weight=4), Good(profit=3, weight=2)]))
    
    
    def test_capacity(self):
        """ This function tests if the solution is feasible"""
        goods = [Good(4, 5), Good(3, 4), Good(3, 2), Good(2, 1)]  # sample data
        capacity = 6
        result_list = knapsack.calc_profit(goods, capacity)[1]
        weight = 0
        for result_good in result_list:
            weight += result_good.weight
        self.assertLessEqual(weight, capacity)
        
        

if __name__ == '__main__':
    unittest.main(verbosity = 2)