﻿"""
Test-suite for each of the tasks in comprehensions.py
This test-suite gave us 2 points. The grader is awesome. ;)
"""

import unittest
import comprehensions


class TestComprehensions(unittest.TestCase):
    """Test class for testing module comprehensions."""
    
    def test_docstring_module(self):
        """This function tests the availability of docstrings."""
        self.assertTrue(comprehensions.__doc__, 'module requires a documentation')
        self.assertTrue(len(comprehensions.__doc__) > 10, 'module requires a more detailed documentation')
    
    
    def test_divisors(self):
        """This function is a test for the divisors of an integer."""
        self.assertEqual(comprehensions.divisors(60), [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 10.0, 12.0, 15.0, 20.0, 30.0, 60.0])     
    
        
    def test_filter_by_first_letter(self):
        """This function is a test the task 'filter by first letter'."""
        strings = ['fortran', 'python', 'javascript', 'c++', 'matlab']
        letter = 'p'
        self.assertEqual(comprehensions.filter_by_first_letter(strings, letter), ['python'])
    
    
    def test_primes(self):
        """This function is a test for primes."""
        self.assertEqual(comprehensions.primes(53), [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53])
    
    
    def test_roll_n_times(self):
        """This function tests the function 'n random integers between 1 and 6'."""
        n = 8
        result = comprehensions.roll_n_times(n)
        length = len(result)
 
        self.assertTrue(length == n)
        for entry in result:
            self.assertGreaterEqual(entry, 1, "not all values greater equal than 1")
            self.assertLessEqual(entry, 6, "not all values less equal than 1")

                    
        

if __name__ == '__main__':
    unittest.main()

