﻿"""
Test for the task simulation.py
"""

import unittest
import simulation


class TestSimulation(unittest.TestCase):
    """Test class for testing module comprehensions."""
    
    def test_docstring_module(self):
        """This function tests the availability of docstrings."""
        self.assertTrue(simulation.__doc__, 'module requires a documentation')
        self.assertTrue(len(simulation.__doc__) > 10, 'module requires a more detailed documentation')
        
        
    
    def test_simulation_devitation(self):
        """does a huge amount of simulation days and tests if the simulated value doesn't differ too much from the expected value"""
        DISTRIBUTION = {0: 0.1, 1: 0.9}
        SIMULATE_DAYS = 10000
        ALLOWED_DEVIATION_PERCENTAGE = 0.1

        test_variable = simulation.simulate_total_demand(DISTRIBUTION, SIMULATE_DAYS)
        expected_value = sum([key * value for key,value in DISTRIBUTION.items()]) * SIMULATE_DAYS

        upper_level = (1 + ALLOWED_DEVIATION_PERCENTAGE) * expected_value
        lower_level = (1 - ALLOWED_DEVIATION_PERCENTAGE) * expected_value
        print('Actual deviation compared with the expected value: {:.2f}%, allowed +-{:.0f}%'.format(
              (1- test_variable/expected_value)*100, ALLOWED_DEVIATION_PERCENTAGE*100))
        self.assertLessEqual(test_variable, upper_level)
        self.assertGreaterEqual(test_variable, lower_level)

    def test_simulation_only_one(self):
        """
        simulates with only one demand (possability: 100%) and another one with possabilty 0%
        simulation should be exactly equal to the expected value"""
        DEMAND_HUNDRED_PERCENT = 1
        DEMAND_ZERO_PERCENT = 50
        SIMULATE_DAYS = 10000
        expected_value = simulation.simulate_total_demand({DEMAND_HUNDRED_PERCENT : 1, DEMAND_ZERO_PERCENT : 0}, SIMULATE_DAYS)
        self.assertEqual(expected_value, DEMAND_HUNDRED_PERCENT * SIMULATE_DAYS)

    def test_read_distribution(self):
        """dont trust gerald"""
        WRONG_PROBABILITIES_FILENAME = 'distribution_wrong.txt'
        self.assertRaises(simulation.ProbabilityDistributionError, simulation.read_distribution, WRONG_PROBABILITIES_FILENAME)

        DISTRIBUTION_IN_FILE = {8 : 0.10, 9 : 0.15, 10 : 0.25, 11 : 0.30, 12 : 0.20 }
        FILENAME = 'distribution.txt'
        self.assertEqual(simulation.read_distribution(FILENAME), DISTRIBUTION_IN_FILE)


if __name__ == '__main__':
    unittest.main(verbosity = 0)