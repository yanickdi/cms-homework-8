﻿"""This module offers a vrptw_writer function as required by hw8, task 1"""

import os

def write_vrpw_customers(customer_list, output_file, sortkey):
    """Writes a given list of customers `customer_list` to an csv file called `output_file`
    
    Arguments`:
    cusomer_list: A list of dictionaries, each containing a customer
    sortkey: A key of the customer dictionaries - The output file is sorted after this key
             Attention! If you want to sort after a special key - all values have to be the same datatype.
             Especially if you want to sort after a column containing numbers - the values have to be floating point or integer numbers, no strings!
    """
    if len(customer_list) <= 0:
        raise ValueError
    #sort the list after the sortkey
    sorted_list = sorted(customer_list, key=lambda val: val[sortkey])
    #write the lines of the list
    with open(output_file, 'w') as f:
        #first line is key line
        keys = ' '.join(sorted_list[0].keys()).strip()
        f.write(keys + '\n')
        for customer in sorted_list:
            line = ' '.join([str(v) for v in customer.values()]) #make values to str() before joing the values to a line
            f.write(line + '\n')