﻿import unittest, tempfile, os.path, sys
import vrptw_reader, vrptw_writer   

class TestVrptwWriter(unittest.TestCase):

    def setUp(self):
        self.big_customer_list = vrptw_reader.read_customer_list_from_file('r101.txt')
        self.output_filename = os.path.join( tempfile.gettempdir(), 'TesVrptwWriterTempFile.tmp.txt')
        try:
            os.remove(self.output_filename)
        except:
            pass

    def test_line_count(self):
        #sort after the first
        keys = list(self.big_customer_list[0].keys())
        vrptw_writer.write_vrpw_customers(self.big_customer_list, self.output_filename, keys[0])
        with open(self.output_filename) as f:
            lines = list(f)
            self.assertEqual(len(lines), len(self.big_customer_list)+1)

    def test_right_order(self):
        #test after key READY_TIME
        vrptw_writer.write_vrpw_customers(self.big_customer_list, self.output_filename, 'READY_TIME')
        output_list = vrptw_reader.read_customer_list_from_file(self.output_filename)
        last_value = output_list[0]['READY_TIME']
        for cust in output_list:
            #last value has to be less or equal than actual (also first compared to first..)
            self.assertLessEqual(last_value, cust['READY_TIME'])
            last_value = cust['READY_TIME']
        

    def test_empty_customer_list(self):
        #empty customer_list should raise a value error
        self.assertRaises(ValueError, vrptw_writer.write_vrpw_customers,
                          [], self.output_filename, None)


if __name__ == '__main__':
    sys.exit(unittest.main(verbosity=2))