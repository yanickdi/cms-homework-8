﻿"""
These functions below consist of list comprehensions.
"""

import random


def divisors(number):
    """Divisors(number) takes a single positive integer and returns a list of its positive divisors.
    >>>divisors(60)
    [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 10.0, 12.0, 15.0, 20.0, 30.0, 60.0]
    """
    divisors_list = [number/_ for _ in range(1, number+1) if number % _ == 0]
    return sorted(divisors_list)



def filter_by_first_letter(strings, letter):
    """Filter_by_first_letter(strings, letter) takes a list of strings and a single character and returns all elements starting with the given letter.
    >>>strings = ['fortran', 'python', 'javascript', 'c++', 'matlab']
    >>>letter = 'j'
    >>>filter_by_first_letter(strings, letter)
    ['javascript']
    """
    filter_list = [element for element in strings if element.startswith(letter)]
    return filter_list



def primes(number):
    """The function primes(number) takes a single integer and returns a list of prime numbers smaller than or equal to that number.
    >>>primes(53)
    [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53]
    """
    primes_list = [_ for _ in range(2, number+1) if all(_ % i != 0 for i in range(2, _))]
    return primes_list



def roll_n_times(n):
    """Roll_n_times(n) takes a single integer and returns a list of n random integers between 1 and 6.
    >>>list = roll_n_times(5)
    >>>len(list)
    5
    """
    random_list=[random.randint(1,6) for _ in range(n)]
    return random_list
