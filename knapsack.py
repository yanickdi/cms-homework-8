#!/usr/bin/env python3

"""
Implementation of a simple recursive function, that returns the maximum profit of packing the rucksack with a subset of the given goods fulfilling the given capacity constraint.
"""

import sys
from collections import namedtuple

Good = namedtuple('Good', ['profit', 'weight'])

def calc_profit(goods, capacity):
    """
    returns two values, first is the maximum profits and the second value is a list of goods
    """
    
    def highest_value(i, remaining_capacity):
        """
        returns the highest profit of the first i good in goods but ignores goods that weight more than the remaining capacity
        
        """
        if i == 0: return 0
        value, weight = goods[i - 1]
        if weight > remaining_capacity:
            return highest_value(i - 1, remaining_capacity)
        else:
            return max(highest_value(i - 1,remaining_capacity),
                highest_value(i - 1, remaining_capacity - weight) + value)
    remaining_capacity = capacity
    result = []
    for i in range(len(goods), 0, -1):
        if highest_value(i, remaining_capacity) != highest_value(i - 1, remaining_capacity):
            result.append(goods[i - 1])
            remaining_capacity -= goods[i - 1][1]
    result.reverse()
    return highest_value(len(goods), capacity), result
  
if __name__ == '__main__':
    goods = [Good(4, 5), Good(3, 4), Good(3, 2), Good(2, 1)]  # sample data
    capacity = 6
    result = calc_profit(goods, capacity)
    print(result[1])
